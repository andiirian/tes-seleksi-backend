const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/";
module.exports = () =>{
  return new Promise((resolve) => {
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      const dbo = db.db("pokemon");
      resolve(dbo)
    });
  })
}
