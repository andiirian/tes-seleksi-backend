const app = require('express')();
const {rateLimitAPI, verifyAuthToken} = require('../middleware')
app.use('/',verifyAuthToken,rateLimitAPI, require('./PokemonRoute'))

module.exports = app;
