const express = require('express');
const router = express.Router();
const {PokemonController} = require('../../controllers')

/* GET home page. */
router.get('/',  PokemonController.getDataPokemon);
router.post('/', PokemonController.postDataPokemon);
module.exports = router;
