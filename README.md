<h3 align="center">Back-End Test Batara Guru Teknologi</h3>

---

<p align="center"> Back-End Engineer Selection Test for Batara Guru Teknologi
    <br> 
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Built Using](#built_using)
- [Authors](#authors)
- [API Documentation](#documentation)

## 🧐 About <a name = "about"></a>

This Project Using NodeJS as Tech Stack for Back-End Engineering Technical Test PT. Batara Guru Teknologi.

## 🏁 Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Before Running This Projects, You Need Install NodeJS version 12.20 MongoDB and Redis

### Installing

After Clone This Project You Need To Run NPM Install Command

```
npm install
```

## 🎈 Usage <a name="usage"></a>

After Installing the Node Modules, You Can Run The Project With NPM Start Command

```
npm run start
```

## ⛏️ Built Using <a name = "built_using"></a>

- [MongoDB](https://www.mongodb.com/) - Database
- [Express](https://expressjs.com/) - Server Framework
- [Redis](https://redis.io/) - Database Cache 
- [NodeJs](https://nodejs.org/en/) - Server Environment

## ✍️ Authors <a name = "authors"></a>

- [@andiirian](https://gitlab.com/andiirian) - Andi Rian

See also the list of [contributors](https://github.com/kylelobo/The-Documentation-Compendium/contributors) who participated in this project.

## 🎉 Acknowledgements <a name = "documentation"></a>

- [Postman API Documentation](https://documenter.getpostman.com/view/5550566/TzRPhTy1)
