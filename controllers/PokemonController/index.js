const db = require('../../database');
const axios = require('axios')
let urlPokemon = null
module.exports = {
  getDataPokemon : async (req, res, next) =>{
    const dbo = await db();
    dbo.collection("karakter").find({}).toArray( (err, result) =>{
      if (err) throw  err;
     res.send({
       status_code : 200,
       data : result
     })
    })
  },
  postDataPokemon : async (req, res, next) =>{
    const dbo = await db();
    if (urlPokemon === null){
      urlPokemon = "https://pokeapi.co/api/v2/pokemon?offset=0&limit=10"
    }
    let dataPokemon = await axios.get(urlPokemon);
    dbo.collection("karakter").insertMany(dataPokemon.data.results, (err, result) =>{
      if (err) throw  err;
      urlPokemon = dataPokemon.data.next;
      res.send({
        status_code : 200,
        msg : 'Created',
        insertedCount : result.insertedCount
      })
    })
  }
}