const redis = require('redis').createClient();
const {verifyToken} = require('../utils/jwt')
module.exports = {
  rateLimitAPI : (req, res, next) =>{
    redis.get(`Ip:${req.ip}`, (err, record) =>{
      redis.get(`Ipaddress:${req.ip}`, (err, data) =>{
        if (err) throw err;
        let dataHitApi = [];
        if (data === null ){
          dataHitApi = [...dataHitApi, {
            endpoint : req.path,
            created_at : new Date()
          }];
        }else{
          data = JSON.parse(data)
          dataHitApi = [...data, {
            endpoint : req.path,
            created_at : new Date()
          }];
        }
        redis.set(`Ipaddress:${req.ip}`, JSON.stringify(dataHitApi));
      });
      if (err) throw err;
      if (record === null) {
        redis.set(`Ip:${req.ip}`, 1, 'EX', 60);
        next();
      }else{
        redis.incr(`Ip:${req.ip}`)
        return (parseInt(record) >= 5) ? res.send({
            status_code : 403,
            msg : 'You have exceeded the 5 requests in 1 minute limit!'
          }) : next();
      }
    })
  },
  verifyAuthToken: (req, res, next) => {
    if (req.headers.token !== undefined) {
      let token = req.headers.token.split(' ')[1];
      verifyToken(token).then(result => {
        req.data = result;

        next();
      }).catch(e => {
        console.error(e);
        res.send({
          status_code: 403,
          msg: 'Auth Failed',
        });
      });
    } else {
      res.send({
        status_code: 403,
        msg: 'Auth Failed',
      });
    }

  },
}