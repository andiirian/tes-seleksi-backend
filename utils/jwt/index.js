let jwt = require('jsonwebtoken');
const JWTSECRET = 'TesSeleksiBE'
module.exports = {
  createdToken: (data) => {
    return new Promise((resolve, reject) => {
      jwt.sign(data, JWTSECRET, (err, token) => {
        return err ? reject(err) : resolve(token);
      });
    });
  },
  verifyToken: (token) => {
    return new Promise((resolve, reject) => {
      jwt.verify(token, JWTSECRET, (err, decoded) => {
        return err ? reject(err) : resolve(decoded);
      });
    });

  },
};